package com.mapnotesapp.mapnotes.ui.main.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.mapnotesapp.mapnotes.R
import com.mapnotesapp.mapnotes.data.model.Mark
import com.mapnotesapp.mapnotes.ui.main.view.MarkListFragmentDirections


class MarksAdapter : RecyclerView.Adapter<MarksAdapter.MarkersViewHolder>() {
    private var markers = mutableListOf<Mark>()


    @SuppressLint("NotifyDataSetChanged")
    fun setMarkerList(markersEntityDBS: MutableList<Mark>) {
        for (i in markersEntityDBS) {
            if (markers.size < markersEntityDBS.size) {
                markers.add(i)
                Log.d("Marker: ", markers.size.toString())
            }
        }
        notifyDataSetChanged()

        Log.d("Agents Final: ", markers.size.toString())
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setMarkerBySearch(markersEntityDBS: MutableList<Mark>, searchMame: String) {
        for (i in markersEntityDBS) {
            if (markers.size < markersEntityDBS.size) {
                if (i.name.lowercase() == searchMame) {
                    markers.add(i)
                    if (markers.size > 1) {
                        markers.removeAt(0)
                    }
                }
                Log.d("Markers: ", markers.size.toString())
            }
        }
        notifyDataSetChanged()
    }


    fun removeMarker(model: Mark) {
        val position = markers.indexOf(model)
        markers.remove(model)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarkersViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.mark_item, parent, false)
        return MarkersViewHolder(view)
    }

    override fun onBindViewHolder(holder: MarkersViewHolder, position: Int) {
        holder.bindData(markers[position])
    }

    override fun getItemCount(): Int {
        return markers.size
    }

    class MarkersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var markerIcon: ImageView = itemView.findViewById(R.id.marker_icon)
        private var markerName: TextView = itemView.findViewById(R.id.m_name)
        private var markerCategory: TextView = itemView.findViewById(R.id.m_category)
        private var markerLatitude: TextView = itemView.findViewById(R.id.m_latitude)
        private var markerLongitude: TextView = itemView.findViewById(R.id.m_longitude)
        private var seeInMap: ImageView = itemView.findViewById(R.id.see_in_map)

        @SuppressLint("SetTextI18n", "UseCompatLoadingForDrawables")
        fun bindData(mark: Mark) {

            markerName.text = mark.name
            markerCategory.text = mark.category
            markerLatitude.text = mark.latitude.toString()
            markerLongitude.text = mark.longitude.toString()

            seeInMap.setOnClickListener {
                val direction =
                    MarkListFragmentDirections.actionMarkListFragmentToMapScreenFragment()
                Navigation.findNavController(itemView).navigate(direction)
            }

            itemView.setOnClickListener {
                val direction =
                    MarkListFragmentDirections.actionMarkListFragmentToMapScreenFragment()
                Navigation.findNavController(itemView).navigate(direction)
            }
        }
    }
}
