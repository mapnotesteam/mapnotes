package com.mapnotesapp.mapnotes.ui.main.view

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.android.gms.maps.GoogleMap
import com.mapnotesapp.mapnotes.R

class LoginScreenFragment: Fragment(R.layout.login_screen_layout){

    lateinit var map: GoogleMap
    lateinit var login_button: Button
    lateinit var register_button: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        login_button = view.findViewById(R.id.login_button)

        register_button = view.findViewById(R.id.register_button)

        login_button.setOnClickListener{
            val direction1 = LoginScreenFragmentDirections.actionLoginScreenFragmentToMapScreenFragment()
            Navigation.findNavController(view).navigate(direction1)
        }

        register_button.setOnClickListener {
            val direction2 = LoginScreenFragmentDirections.actionLoginScreenFragmentToRegisterScreenFragment()
            Navigation.findNavController(view).navigate(direction2)
        }
    }
}