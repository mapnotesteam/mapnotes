package com.mapnotesapp.mapnotes.ui.main.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mapnotesapp.mapnotes.data.model.Mark

class MarksViewModel(): ViewModel() {
    val agents = MutableLiveData<MutableList<Mark>>()

    fun getMarkers(): MutableLiveData<MutableList<Mark>> {
        return agents
    }
}
