package com.mapnotesapp.mapnotes.ui.main.view

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.mapnotesapp.mapnotes.R
import com.mapnotesapp.mapnotes.ui.main.adapter.MarksAdapter
import com.mapnotesapp.mapnotes.ui.main.viewModel.MarksViewModel

class MarkListFragment: Fragment(R.layout.mark_list_layout)  {

    private lateinit var myMarksText : TextView
    private lateinit var searchText: TextInputLayout
    private lateinit var markSearch: TextInputEditText
    private lateinit var goSearchButton: Button
    private lateinit var addMarkButton: FloatingActionButton

    private lateinit var marksRecyclerView: RecyclerView
    private val marksViewModel: MarksViewModel by activityViewModels()
    private val adapter = MarksAdapter()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        myMarksText = view.findViewById(R.id.my_marks_text)
        searchText = view.findViewById(R.id.searchText)
        markSearch = view.findViewById(R.id.markSearch)
        goSearchButton = view.findViewById(R.id.gosearch_button)
        addMarkButton = view.findViewById(R.id.add_mark_button)

        marksRecyclerView = view.findViewById(R.id.marks_recycle_view)

        addMarkButton.setOnClickListener {
            val latitude = 0
            val longitude = 0
            val direction = MarkListFragmentDirections.actionMarkListFragmentToAddMarkFragment(latitude.toFloat(), longitude.toFloat())
            Navigation.findNavController(view).navigate(direction)
        }

        marksRecyclerView.layoutManager = LinearLayoutManager(context)

        marksRecyclerView.adapter = adapter

        marksViewModel.getMarkers().observe(viewLifecycleOwner, Observer { item ->
            // Update the UI
            item.let { adapter.setMarkerList(it!!) }
        })
    }
}