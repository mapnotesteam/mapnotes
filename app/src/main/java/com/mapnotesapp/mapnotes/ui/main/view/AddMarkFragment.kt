package com.mapnotesapp.mapnotes.ui.main.view

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.mapnotesapp.mapnotes.R

class AddMarkFragment: Fragment(R.layout.add_marker_layout) {

    lateinit var add_button: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        add_button = view.findViewById(R.id.register_button)

        add_button.setOnClickListener {
            val direction = AddMarkFragmentDirections.actionAddMarkFragmentToMapScreenFragment()
            Navigation.findNavController(view).navigate(direction)
        }
    }
}