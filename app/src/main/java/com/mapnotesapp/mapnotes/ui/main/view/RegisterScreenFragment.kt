package com.mapnotesapp.mapnotes.ui.main.view

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.google.firebase.firestore.FirebaseFirestore
import com.mapnotesapp.mapnotes.R

class RegisterScreenFragment: Fragment(R.layout.register_screen_layout) {

    lateinit var register_button: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        register_button = view.findViewById(R.id.register_button)

        register_button.setOnClickListener {
            val direction = RegisterScreenFragmentDirections.actionRegisterScreenFragmentToLoginScreenFragment()
            Navigation.findNavController(view).navigate(direction)
        }
    }
}