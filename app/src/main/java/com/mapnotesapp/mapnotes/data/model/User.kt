package com.mapnotesapp.mapnotes.data.model

data class User(
    val id: Long,
    val name: String,
    val email: String
)
