package com.mapnotesapp.mapnotes.data.model

data class Mark(
    val id: Long,
    val name: String,
    val latitude: Double,
    val longitude: Double,
    val category: String
//    val image: ImageView,
)


